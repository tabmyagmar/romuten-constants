const ErrorTypes = {
  ERROR: "ERROR",
  WARNING: "WARNING",
  SUCCESS: "SUCCESS",
  ERROR_SMARTHR: "ERROR_SMARTHR",
};

const UploadType = {
  TEMP: "temp",
  ADMIN: "admin",
  COMPANY: "company",
  CLIENT: "client",
  STAFF: "staff",
  SUPPORT: "support",
  HELLOWORK_CSV: "helloworkCsv",
  HELLOWORK_PDF: "helloworkPdf",
  CONTRACT_TEMPLATE: "contractTemplate",
  STAFF_CONTRACT: "staffContract",

  // CSV MANAGE
  ADMIN_CSV_UPLOAD: "adminCsvUpload",
  BRANCH_CSV_UPLOAD: "branchCsvUpload",
  BRANCH_CSV_UPDATE: "branchCsvUpdate",
  COMPANY_CSV_UPLOAD: "companyCsvUpload",
  USER_CSV_UPLOAD: "userCsvUpload",
  STAFF_CSV_UPLOAD: "staffCsvUpload",
};

const FileTypeWhiteList = [
  { extension: "jpeg" },
  { extension: "png" },
  { extension: "jpg" },
  { extension: "csv" },
  { extension: "pdf" },
];

const CardTypes = Object.freeze({
  SPECIAL_PERMANENT_RESIDENT: 50,
  CARD_RESIDENT: 49,
});
// ENUMVALUES
const GROUP_TYPES_CODE = Object.freeze({
  admin: 100001,
  company: 100002,
  client: 100003,
  member: 100004,
});

const USER_STATUS_CODE = Object.freeze({
  delete: 200001,
  pending: 200002,
  active: 200003,
});

const STAFF_STATUS_CODE = Object.freeze({
  checking: 220001,
  hire: 220002,
  employmentNotificationNotSubmitted: 220003,
  submittedEmploymentNotice: 220004,
  retire: 220005,
  notSubmittedTurnoverNotice: 220006,
  submittedTurnoverNotice: 220007,
  notRecruited: 220008,
  inviteStaff: 220009,
  uploadStaff: 220010,
  csvUpload: 220011,
  smarthr: 220012,
});

const USER_GERDER_CODE = Object.freeze({
  female: 210001,
  male: 210002,
});

const PERM_INDIVIDUAL_CODE = Object.freeze({
  have: 230001,
  notHave: 230002,
});

const PERM_COMPREHENSIVE_CODE = Object.freeze({
  have: 240001,
  notHave: 240002,
});

const UPDATE_STATUS_CODE = Object.freeze({
  applyingForRenewal: 250001,
  otherThanApplyingForRenewal: 250002,
});

const RESIDENCE_CARD_TYPE_CODE = Object.freeze({
  residenceCard: 260001,
  eigyuCard: 260002,
});

const DRIVERS_LICENSE_TYPE_CODE = Object.freeze({
  heavyVehicle: 270001,
  mediumVehicle: 270002,
  semiMediumVehicle: 270003,
  ordinaryVehicle: 270004,
  heavySpecialVehicle: 270005,
  smallSpecialVehicle: 270006,
  heavyMotorcycle: 270007,
  ordinaryMotorcycle: 270008,
  smallMotorcycle: 270009,
  moped: 270010,
});

const CARD_RESIDENCE_STATUS_CODE = Object.freeze({
  updateRequired: 280001,
  updating: 280002,
  updateDone: 280003,
  updateConfirmed: 280004,
  confirmNeeded: 280005,
  visaTypeChanging: 280006,
});

const COMPANY_STATUS_CODE = Object.freeze({
  deleted: 410001,
  pending: 410002,
  active: 410003,
});

const CLIENT_STATUS_CODE = Object.freeze({
  delete: 510001,
  pending: 510002,
  active: 510003,
});

const ADMIN_STATUS_CODE = Object.freeze({
  delete: 610001,
  pay: 610002,
  finish: 610003,
  active: 610004,
});

const TIMER_TYPE_CODE = Object.freeze({});

const SEND_TO_CODE = Object.freeze({
  admin: 710001,
  company: 710002,
  client: 710003,
  staff: 710004,
  system: 710005,
  noting: 710006,
});

const TEMPLATE_TYPE_CODE = Object.freeze({
  staff: 720000,
  system: 720001,
  other: 720002,
  residenceCard: 720003,
  helloworkCard: 720004,
  docWorkpermission: 720005,
  cardStudent: 720006,
  cardMyNumber: 720007,
  cardDriverLicense: 720008,
  docPassport: 720009,
  docStudentLongTime: 720010,
  residenceUpdating: 720011,
});

const CRON_TYPE_CODE = Object.freeze({
  cardResidence: 730001,
  helloworkEmployment: 730002,
  helloworkUnEmployment: 730003,
  docworkPermission: 730004,
  cardMynumber: 730005,
  cardStudent: 730006,
  cardDriverlicense: 730007,
  docPassport: 730008,
  docStudentlongtime: 730009,
  cardResidenceupdating: 730010,
});

const NOTIFICATION_TYPE_CODE = Object.freeze({
  notification: 810001,
  caution: 810002,
  warning: 810003,
});

const CONTRACT_TYPE_CODE = Object.freeze({
  writtenOath: 820001,
  privacyPolicyAgreement: 820002,
});

const LOG_ACTION_TYPE_CODE = Object.freeze({
  update: 830001,
  create: 830002,
  delete: 830003,
  read: 830004,
});

const LOG_SEVERITY_CODE = Object.freeze({
  error: 840001,
  warning: 840002,
  notice: 840003,
  info: 840004,
  debug: 840005,
});

const LOG_RESPONSE_CODE = Object.freeze({
  success: 850001,
  systemError: 850002,
  error: 850003,
  accessError: 850004,
});

const AUDIT_LOG_ENUMTYPE_CODE = Object.freeze({
  targer: 860001,
  action: 860002,
  event: 860003,
});

const AUDIT_LOG_LOCATION_CODE = Object.freeze({
  mobile: 870001,
  web: 870002,
});

const CSV_UPLOAD_STATUS_CODE = Object.freeze({
  success: 880001,
  failed: 880002,
});
// MODEL ENUM
// MODEL: MultiHistoryPush
const MULTI_PUSH_STATUS_ENUM = Object.freeze({
  error: "ERROR",
  pending: "PENDING",
  sent: "SENT",
  finish: "FINISH",
});
// MODEL: MultiHistoryMail
const MULTI_MAIL_STATUS_ENUM = Object.freeze({
  error: "ERROR",
  pending: "PENDING",
  sent: "SENT",
  finish: "FINISH",
});

const CRON_CARDTYPES_ENUM = Object.freeze({
  cardDriver: "CARDDRIVER",
  cardMyNumber: "CARDMYNUMBER",
  cardNfcUpdating: "CARDNFCUPDATING",
  cardResidence: "CARDRESIDENCE",
  cardStudent: "CARDSTUDENT",
  docPassport: "DOCPASSPORT",
  docStudentLong: "DOCSTUDENTLONG",
});

module.exports = {
  ErrorTypes,
  UploadType,
  FileTypeWhiteList,
  CardTypes,
  // ENUMVALUES
  GROUP_TYPES_CODE,
  USER_STATUS_CODE,
  STAFF_STATUS_CODE,
  USER_GERDER_CODE,
  PERM_INDIVIDUAL_CODE,
  PERM_COMPREHENSIVE_CODE,
  UPDATE_STATUS_CODE,
  RESIDENCE_CARD_TYPE_CODE,
  DRIVERS_LICENSE_TYPE_CODE,
  CARD_RESIDENCE_STATUS_CODE,
  COMPANY_STATUS_CODE,
  CLIENT_STATUS_CODE,
  ADMIN_STATUS_CODE,
  TIMER_TYPE_CODE,
  SEND_TO_CODE,
  TEMPLATE_TYPE_CODE,
  CRON_TYPE_CODE,
  NOTIFICATION_TYPE_CODE,
  CONTRACT_TYPE_CODE,
  LOG_ACTION_TYPE_CODE,
  LOG_SEVERITY_CODE,
  LOG_RESPONSE_CODE,
  AUDIT_LOG_ENUMTYPE_CODE,
  AUDIT_LOG_LOCATION_CODE,
  CSV_UPLOAD_STATUS_CODE,
  // MODEL ENUM
  MULTI_PUSH_STATUS_ENUM,
  MULTI_MAIL_STATUS_ENUM,
  CRON_CARDTYPES_ENUM,
};
